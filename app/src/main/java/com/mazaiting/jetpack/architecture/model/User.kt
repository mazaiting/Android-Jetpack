package com.mazaiting.jetpack.architecture.model

/**
 * 实体类
 * Created by mazaiting on 2018/7/25.
 */
data class User(
        var firstName: String,
        var lastName: String
)