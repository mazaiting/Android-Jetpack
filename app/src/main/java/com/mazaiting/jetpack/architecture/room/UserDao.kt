package com.mazaiting.jetpack.architecture.room

import android.arch.persistence.room.*

/**
 * 用户DAO类
 * Created by mazaiting on 2018/7/25.
 */
@Dao
interface UserDao {
  @Query("SELECT * FROM user")
  fun getAll(): List<User>
  @Query("SELECT * FROM user  WHERE uid IN (:userIds)")
  fun loadAllByIds(userIds: IntArray): List<User>
  @Query("SELECT * FROM user WHERE first_name LIKE :first AND last_name LIKE :last LIMIT 1")
  fun findByName(first: String, last: String): User
  @Insert
  fun insertAll(users: List<User>)
  // 可变参数
//  fun insertAll(vararg users: User)
  @Delete
  fun delete(user: User)
  @Update
  fun update(user: User)
}