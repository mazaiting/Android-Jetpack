package com.mazaiting.jetpack.architecture.model

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.mazaiting.jetpack.R
import com.mazaiting.log.L
import kotlinx.android.synthetic.main.activity_view_model.*

class ViewModelActivity : AppCompatActivity() {
  
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_view_model)
    
    btn_model_get_data.setOnClickListener {
      val model: MyViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(application).create(MyViewModel::class.java)
      model.users?.observe(this, Observer { users ->
        users?.forEach {
          L.e(it.toString())
        }
      })
    }
  }
}
