package com.mazaiting.jetpack.architecture.lifecycle.life

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.mazaiting.jetpack.R

class SpecialLifeCycleActivity : AppCompatActivity() {
  
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_special_life_cycle)
    val presenter = SpecialPresenter()
    // 添加观察者
    lifecycle.addObserver(presenter)
  }
}
