package com.mazaiting.jetpack.architecture.navigation.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation
import com.mazaiting.jetpack.R

class BlankFragment2 : Fragment() {
  
  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                            savedInstanceState: Bundle?): View? {
    // Inflate the layout for this fragment
    val view = inflater.inflate(R.layout.fragment_blank_fragment2, container, false)
    val btnShow: Button = view.findViewById(R.id.btn_show_fragment2)
    btnShow.setOnClickListener { btnView ->
      // 返回上一页
      Navigation.findNavController(btnView).navigateUp()
    }
    return view
  }
}
