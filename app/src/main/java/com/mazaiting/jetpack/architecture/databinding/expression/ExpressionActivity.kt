package com.mazaiting.jetpack.architecture.databinding.expression

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.mazaiting.jetpack.R
import com.mazaiting.jetpack.architecture.databinding.bean.User
import com.mazaiting.jetpack.databinding.ActivityExpressionBinding

class ExpressionActivity : AppCompatActivity() {
  private lateinit var user: User
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    val binding = DataBindingUtil.setContentView<ActivityExpressionBinding>(this, R.layout.activity_expression)
    user= User("ma", "zaiting", 1, null)
    // 要在layout中使用，一定要绑定
    binding.user = user
    binding.presenter = ExpressionPresenter()
  }
  
  // 发现修改无效
  fun onChangeFirstName(view: View) {
    user.firstName = "aaa"
  }
}
