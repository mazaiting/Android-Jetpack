package com.mazaiting.jetpack.architecture.room

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * 实体类
 * Created by mazaiting on 2018/7/25.
 */
@Entity
data class User(
       @PrimaryKey
        val uid: Int,
       @ColumnInfo(name = "first_name")
       var firstName: String,
       @ColumnInfo(name = "last_name")
       var lastName: String
)