package com.mazaiting.jetpack.architecture.lifecycle

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.mazaiting.jetpack.R
import com.mazaiting.jetpack.architecture.lifecycle.custom.CustomLifeCycleActivity
import com.mazaiting.jetpack.architecture.lifecycle.life.SpecialLifeCycleActivity
import kotlinx.android.synthetic.main.activity_life_cycle.*

class LifeCycleActivity : AppCompatActivity() {
  
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_life_cycle)
  
    btn_life_cycle_custom.setOnClickListener { startActivity(Intent(this, CustomLifeCycleActivity::class.java)) }
    btn_life_cycle_special.setOnClickListener { startActivity(Intent(this, SpecialLifeCycleActivity::class.java)) }
  }
}
