package com.mazaiting.jetpack.architecture.navigation.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation
import com.mazaiting.jetpack.R

class BlankFragment : Fragment() {
  
  override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                            savedInstanceState: Bundle?): View? {
    // Inflate the layout for this fragment
    val view = inflater.inflate(R.layout.fragment_blank, container, false)
    val btnShow: Button = view.findViewById(R.id.btn_show_fragment)
    btnShow.setOnClickListener { btnView ->
      // 跳转下一个页面
      Navigation.findNavController(btnView).navigate(R.id.action_blankFragment_to_blankFragment2)
    }
    return view
  }
  
}
