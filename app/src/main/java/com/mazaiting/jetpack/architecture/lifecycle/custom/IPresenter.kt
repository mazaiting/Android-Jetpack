package com.mazaiting.jetpack.architecture.lifecycle.custom

/**
 * Created by mazaiting on 2018/7/27.
 */
interface IPresenter {
  fun onCreate()
  fun onStart()
  fun onResume()
  fun onPause()
  fun onStop()
  fun onDestroy()
}