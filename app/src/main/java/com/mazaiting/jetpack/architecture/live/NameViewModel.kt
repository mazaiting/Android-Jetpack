package com.mazaiting.jetpack.architecture.live

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

/**
 * Created by mazaiting on 2018/7/25.
 */
class NameViewModel : ViewModel() {
  // Create LiveData Objects
  var currentName : MutableLiveData<String>? = null
    get() {
      if (null == field) {
        currentName = MutableLiveData()
      }
      return field
    }
  
}