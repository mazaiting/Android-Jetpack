package com.mazaiting.jetpack.architecture.databinding.bean

import android.databinding.ObservableField
import android.databinding.ObservableInt

/**
 * 可改变的实体类
 * Created by mazaiting on 2018/7/24.
    ObservableBoolean
    ObservableByte
    ObservableChar
    ObservableShort
    ObservableInt
    ObservableLong
    ObservableFloat
    ObservableDouble
    ObservableParcelable
    ObservableField
 */
data class ObservableUser(
        val firstName: ObservableField<String>,
        val lastName: ObservableField<String>,
        val age: ObservableInt
)